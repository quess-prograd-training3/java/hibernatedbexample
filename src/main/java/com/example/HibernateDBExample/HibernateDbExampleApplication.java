package com.example.HibernateDBExample;


import com.example.HibernateDBExample.component.Student;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import java.io.File;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class HibernateDbExampleApplication {

	public static void main(String[] args) {

		//String is used to store the configurationFile name along with extension
		String ConfigurationFile = "hibernate.cfg.xml";
		//below lines are used to fetch the session factory from resources folder and
		// get the session factory object in the class where u want to load it
		ClassLoader classLoaderObject=HibernateDbExampleApplication.class.getClassLoader();
		File file=new File(classLoaderObject.getResource(ConfigurationFile).getFile());
		//the session factory class object from configuration file is fetched
		// and assigned to session factory object
		SessionFactory sessionFactoryObject=new Configuration().configure().buildSessionFactory();
		Session sessionObject=sessionFactoryObject.openSession();
		//saveRecord(sessionObject);
		update(sessionObject);
		delete(sessionObject);
		display(sessionObject);
	}
	public static void display(Session sessionObj){
		Query queryObj=sessionObj.createQuery("FROM Student");
		List<Student> listObj=queryObj.list();
		for(Student iterate:listObj){
			System.out.println(iterate.getId()+" "+iterate.getsName()+" "+iterate.getRollNo()+" "+iterate.getStd());
		}
	}
	public static void update(Session sessionObj){
		int id=2;
		Student studentObj=(Student) sessionObj.get(Student.class,id);
		studentObj.setsName("MohanSai");
		sessionObj.beginTransaction();
		sessionObj.saveOrUpdate(studentObj);
		sessionObj.getTransaction().commit();
		System.out.println("Update Record");
	}
	public static void delete(Session sessionObj){
		int id=6;
		Student studentObj=(Student) sessionObj.get(Student.class,id);
		sessionObj.beginTransaction();
		sessionObj.delete(studentObj);
		sessionObj.getTransaction().commit();
		System.out.println("delete Record");
	}
	public static void saveRecord(Session sessionObj){
		Student student=new Student();
		/*student.setId(3);
		student.setsName("chanti");
		student.setRollNo(21);
		student.setStd(15);*/
		student.initialize();
		sessionObj.beginTransaction();
		sessionObj.save(student);
		sessionObj.getTransaction().commit();
		System.out.println("Record added");
	}

}
