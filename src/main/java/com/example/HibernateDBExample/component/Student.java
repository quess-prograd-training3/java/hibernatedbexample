package com.example.HibernateDBExample.component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Scanner;

@Entity
@Table(name="Student")
public class Student {
    @Id
    @Column(name="id")
    private int id;
    @Column(name="sName")
    private String sName;
    private int rollNo;
    private int std;

    public Student() {
    }

    public Student(int id, String sName, int rollNo, int std) {
        this.id = id;
        this.sName = sName;
        this.rollNo = rollNo;
        this.std = std;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public int getStd() {
        return std;
    }

    public void setStd(int std) {
        this.std = std;
    }
    public void initialize(){
        Scanner scanner=new Scanner(System.in);
        id=scanner.nextInt();
        sName=scanner.next();
        rollNo=scanner.nextInt();
        std=scanner.nextInt();
    }
}
